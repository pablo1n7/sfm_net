import pandas as pd
import numpy as np
import theano
import lasagne
from lasagne.updates import nesterov_momentum
from nolearn.lasagne import NeuralNet
from lasagne.layers import InputLayer, Conv2DLayer, DropoutLayer,\
                           MaxPool2DLayer, DenseLayer, ReshapeLayer
from lasagne.objectives import categorical_crossentropy
from nolearn.lasagne import BatchIterator
from sklearn.cross_validation import train_test_split
from sklearn.preprocessing import MinMaxScaler
import cPickle as pickle
import sys  

reload(sys)  
sys.setdefaultencoding('utf8')
sys.setrecursionlimit(40000)

class EarlyStopping(object):
    def __init__(self, patience=100):
        self.patience = patience
        self.best_valid = np.inf
        self.best_valid_epoch = 0
        self.best_weights = None

    def __call__(self, nn, train_history):
        current_valid = train_history[-1]['valid_loss']
        current_epoch = train_history[-1]['epoch']
        if current_valid < self.best_valid:
            self.best_valid = current_valid
            self.best_valid_epoch = current_epoch
            self.best_weights = nn.get_all_params_values()
        elif self.best_valid_epoch + self.patience < current_epoch:
            print("Early stopping.")
            print("Best valid loss was {:.6f} at epoch {}.".format(
                self.best_valid, self.best_valid_epoch))
            nn.load_params_from(self.best_weights)
            raise StopIteration()

class FlipBatchIterator(BatchIterator):
    def transform(self, Xb, yb):
        """
        Flip randomly half of the images and their associated landmarks
        """
        Xb, yb = super(FlipBatchIterator, self).transform(Xb, yb)

        bs = Xb.shape[0]
        indices = np.random.choice(bs, bs//2, replace=False)
        Xb[indices] = Xb[indices, :, :, ::-1]

        if yb is not None:
            yb[indices, ::2] = yb[indices, ::2] * -1

        return Xb, yb

class AdjustVariable(object):
    """
    Used to decreases linearly the learning rate with the number of epochs,
    while we the momentum increase.
    """
    def __init__(self, name, start=0.03, stop=0.001):
        self.name = name
        self.start, self.stop = start, stop
        self.ls = None

    def __call__(self, nn, train_history):
        if self.ls is None:
            self.ls = np.linspace(self.start, self.stop, nn.max_epochs)

        epoch = train_history[-1]['epoch']
        new_value = np.float32(self.ls[epoch - 1])
        getattr(nn, self.name).set_value(new_value)

def create_network(npochs=1000, batch_s=2000):
    layers_0 = [
                (InputLayer, {'shape': (None, 1, 128, 128)}),
                (Conv2DLayer, {'num_filters': 32, 'filter_size': (4, 4)}),
                (Conv2DLayer, {'num_filters': 32, 'filter_size': (4, 4)}),
                (MaxPool2DLayer, {'pool_size': 2}),
                (DropoutLayer, {'p': 0.1}),
                (Conv2DLayer, {'num_filters': 32, 'filter_size': (3, 3)}),
                (Conv2DLayer, {'num_filters': 32, 'filter_size': (3, 3)}),
                (MaxPool2DLayer, {'pool_size': 2}),
                (DropoutLayer, {'p': 0.2}),
                (Conv2DLayer, {'num_filters': 64, 'filter_size': (3, 3)}),
                (Conv2DLayer, {'num_filters': 64, 'filter_size': (3, 3)}),
                (MaxPool2DLayer, {'pool_size': 2}),        
                (DropoutLayer, {'p': 0.3}),
                (DenseLayer, {'num_units': 1500}),
                (DropoutLayer, {}),
                (DenseLayer, {'num_units': 1500}),
                (DenseLayer, {'num_units': 200, 'nonlinearity': None}),
        ]
    return NeuralNet(
        layers=layers_0,
        update=nesterov_momentum,
        update_learning_rate=theano.shared(np.float32(0.001)),
        update_momentum=theano.shared(np.float32(0.9)),

        regression=True,
        batch_iterator_train=FlipBatchIterator(batch_size=batch_s),
        on_epoch_finished=[
            AdjustVariable('update_learning_rate', start=0.08, stop=0.001),
            AdjustVariable('update_momentum', start=0.9, stop=0.9999),
            EarlyStopping(patience=1000),
        ],
        max_epochs=npochs,
        verbose=1)


import glob
csvs =  np.array(glob.glob("*.csv"))
np.random.shuffle(csvs)
data = []
for cvs in csvs[:3]:
    df = pd.read_csv(cvs,iterator=True, chunksize=1000)
    _data = pd.concat([chunk for chunk in df])
    data.append(_data)

del df,_data

data = pd.concat(data)
print(data.shape)

np.random.shuffle(data)

data_X = np.array(data["Image"])
data_Y = np.array(data["SURF"])
del data
# Preparando los datos

X  = np.array(list(map(lambda X: np.fromstring(X,sep=" ").reshape(128,128) ,data_X)),dtype=np.float32)
y = np.array(list(map(lambda X: np.fromstring(X,sep=" ").reshape(100,2) ,data_Y)),dtype=np.float32)
y = y.reshape((y.shape[0],y.shape[1]*2))
y = np.array(list(map(lambda x: MinMaxScaler().fit_transform(x),y)))
X = np.array(list(map(lambda x: MinMaxScaler().fit_transform(x),X)))

X = X.reshape(-1, 1, 128, 128)

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3,random_state=42)
del X,y

net = create_network(10000)
net.fit(X_train, y_train)

from sklearn.metrics import mean_squared_error, mean_absolute_error, explained_variance_score, r2_score
y_pred = net.predict(X_test)
print np.sqrt(mean_squared_error(y_test, y_pred))
print explained_variance_score(y_test.ravel(), y_pred.ravel())
#print mean_absolute_error(y_test, y_pred)
print r2_score(y_test, y_pred) 

with open('net_10000_.pickle', 'wb') as f:
    pickle.dump(net, f, -1)
