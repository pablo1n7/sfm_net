import sh
import glob
    
def main():
    ffmpeg = sh.Command("/bin/ffmpeg")
    list_videos = glob.glob("/home/pablo/Dataset/videos_CITES/*/*.mp4")
    list_videos.sort()
    for l_v in list_videos:
        folder = l_v.replace(".mp4","")
        sh.mkdir(folder)
        ffmpeg("-i", l_v, "-vf", "fps=1", "{}/out%d.png".format(folder))

if __name__ == '__main__':
    main()

