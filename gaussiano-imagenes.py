import sh
import glob
import cv2
    
def main():
    list_pngs = glob.glob("/home/pablo/Dataset/videos_CITES/*/*/*.png")
    for f_png in list_pngs:
        img = cv2.imread(f_png,0) 
        frame = cv2.GaussianBlur(img,(0,0),5)
        frame = cv2.addWeighted(img,1.5,frame,-0.5,0)
        cv2.imwrite(f_png,frame)
                
if __name__ == '__main__':
    main()
