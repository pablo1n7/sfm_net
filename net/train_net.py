from torchvision import transforms
from torch.utils.data import DataLoader
from torch.autograd import Variable
from torch.optim.lr_scheduler import *
import matplotlib.pyplot as plt
from SurfDataset import *
from Utils import *
from AlexnetNetwork import *
from glob import glob
from sklearn.metrics import r2_score

transform = transforms.Compose([ToTensor()])
surf_dataset = SurfDataset(csv_file='../dataset_apache_train.csv', transform=transform, img_size=224)
surf_dataset_validation = SurfDataset(csv_file='../dataset_apache_validate.csv', transform=transform, img_size=224)

def main():
    net = AlexnetNetwork().cuda()
    criterion = torch.nn.MSELoss().cuda()
    optimizer = torch.optim.SGD(net.parameters(), lr=0.00000001, momentum=0.9, nesterov=True)
    scheduler = ReduceLROnPlateau(optimizer, 'min', verbose= True)
    files_checkpoints = np.array(sorted(glob("../checkpoints/*.pkl")))
    if files_checkpoints.shape[0] != 0:
        net.load_state_dict(torch.load(files_checkpoints[-1]))
        print(files_checkpoints[-1])

    plotter = VisdomLinePlotter(env_name="main")
    
    vis_dataloader = DataLoader(surf_dataset,
                                shuffle=True,
                                num_workers=4,
                                batch_size=1024)
    
    vis_dataloader_validation = DataLoader(surf_dataset_validation,
                                shuffle=True,
                                num_workers=4,
                                batch_size=1024)
    
    
    
    for epoch in range(10000):

        """train"""
        
        acum_loss = 0
        optimizer.zero_grad()
        for i, data in enumerate(vis_dataloader):
            #optimizer.zero_grad()
            img = data["image"]
            label = data["landmarks"]
            img, label = Variable(img).cuda(), Variable(label).cuda()
            output = net(img.view(-1, 3, 224, 224))
            loss = criterion(output.view(-1, 100), label.view(-1, 100))
            loss.backward()
            optimizer.step()
            acum_loss += loss.data[0]
        #optimizer.step()
        train_loss_epoch = acum_loss  / (i+1)
        plotter.plot('Epochs loss', "Train loss", epoch, train_loss_epoch, 'Epochs')
        print("-" * 20)
        print("Epoch - {} - training loss {} ".format(epoch, train_loss_epoch))

        """validation"""
        
        acum_loss = 0
        for i, data in enumerate(vis_dataloader_validation):
            img = data["image"]
            label = data["landmarks"]
            img, label = Variable(img).cuda(), Variable(label).cuda()
            output = net(img.view(-1, 3, 224, 224))
            loss = criterion(output.view(-1, 100),label.view(-1, 100)).data[0]
            acum_loss += loss
            scheduler.step(loss)

        validation_loss_epoch = acum_loss / (i+1)
        #scheduler.step(validation_loss_epoch)
        plotter.plot('Epochs loss', "Valid loss", epoch, validation_loss_epoch, 'Epochs')
        print("validation loss {}".format(validation_loss_epoch))
        print("-" * 20)
        torch.save(net.state_dict(), "../checkpoints/{}_+{}.pkl".format("epoch", 1))
        
    torch.save(net.state_dict(), "../checkpoints/{}_+{}.pkl".format("checkpoints", 1200))



if __name__ == '__main__':
    main()
