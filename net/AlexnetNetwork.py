from torch.autograd import Variable
from torchvision import models
import torch.nn as nn
import numpy as np
import torch   

original_model = models.alexnet(pretrained=True)

class AlexnetNetwork(nn.Module):
    def __init__(self):
        super(AlexnetNetwork, self).__init__()
        self.features = nn.Sequential(
                *list(original_model.features.children())[0:6]
        )
        self.classifier = nn.Sequential(
            # stop at fc8
            *list(original_model.classifier.children())[:-5],
        )
        self.dimreduction = nn.Sequential(
            nn.Linear(32448, 16384),
            #nn.Linear(4096, 1024),
            #nn.Linear(1024, 100)
        )
        self.cnn_1 = nn.Sequential(
            nn.Conv2d(1, 32, (4, 4)),
            nn.Conv2d(32, 32, (4, 4)),
            nn.MaxPool2d(2),
            nn.Dropout2d(p=.1),
            nn.Conv2d(32, 32, (3, 3)),
            nn.Conv2d(32, 32, (3, 3)),
            nn.MaxPool2d(2),
            nn.Dropout2d(p=.2),
            nn.Conv2d(32, 64, (3, 3)),
            nn.Conv2d(64, 64, (3, 3)),
            nn.MaxPool2d(2),
            nn.Dropout2d(p=.3)
        )

    def forward(self, x):
        output = self.features(x)
        #output = output.view(output.size(0), 256 * 6 * 6)
        #output = self.classifier(output)
        print(output.size())
        output = self.dimreduction(output.view(-1, 32448))
        output = self.cnn_1(output.view(-1, 1, 128, 128))
        return output   
    
