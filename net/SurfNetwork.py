from torch.autograd import Variable
import torch.nn as nn
import torch

class SurfNetwork(nn.Module):
    def __init__(self, img_size=96):
        super(SurfNetwork, self).__init__()
        self.img_size = img_size
        self.cnn_1 = nn.Sequential(
            nn.Conv2d(1, 32, (4, 4)),
           # nn.Conv2d(32, 32, (4, 4)),
            nn.MaxPool2d(2),
            #nn.Dropout2d(p=.1),
            nn.Conv2d(32, 32, (3, 3)),
            #nn.Conv2d(32, 32, (3, 3)),
            nn.MaxPool2d(2),
            #nn.Dropout2d(p=.2),
            nn.Conv2d(32, 64, (3, 3)),
            #nn.Conv2d(64, 64, (3, 3)),
            nn.MaxPool2d(2),
            #nn.Dropout2d(p=.3)
            )
        self.cnn_2 = nn.Sequential(
            nn.Linear(6400, 1500),
            #nn.Dropout2d(),
            #nn.Linear(1500, 1500),
            nn.Linear(1500, 100)
        )

    def forward(self, x):
        #outputs = Variable(torch.zeros(batch_size, 1, 200)).cuda()
        #xs = xs.view(batch_size, 1, self.img_size, self.img_size)
        #for i, x in enumerate(xs):
           # x = x.view(1, 1, self.img_size, self.img_size)
        output = self.cnn_1(x.view(-1, 1, 96, 96))
        output = output.view(output.size()[0], -1)
            #print(output.size())
        output = self.cnn_2(output)
        return output
        #outputs[i] = output
        #return outputs
