from torch.utils.data import Dataset
import torch
import pandas as pd
import numpy as np
import cv2

from scipy.stats import rv_discrete
from sklearn.preprocessing import MinMaxScaler

class ToTensor(object):
    """Convert ndarrays in sample to Tensors."""

    def __call__(self, sample):
        image, landmarks = sample['image'], sample['landmarks']

        return {'image': torch.Tensor(image),
                'landmarks': torch.Tensor(landmarks)}

class SurfDataset(Dataset):
    """ Landmarks dataset."""

    def __init__(self, csv_file, transform=None, img_size=96):
        self.landmarks_frame = pd.read_csv(csv_file, index_col=0, header=0).reset_index()
        self.transform = transform
        self.img_size = img_size
        self.scaler = MinMaxScaler(feature_range=(-1, 1))
        data = np.array(self.landmarks_frame)[:,range(2,102)]
        data = np.array(data,dtype=np.float64)
        scaler = self.scaler.fit(data)
        self.prob = rv_discrete("T", values=([[0, 1, 2, 3],(0.25, 0.25, 0.25, 0.25)]))
    

    def __len__(self):
        return len(self.landmarks_frame)

    def flip(self, img, kps):
        refl_matrix = np.array([[-1, 0], [0, -1]])
        kps_r = kps.dot(refl_matrix) + self.img_size
        return np.flip(np.flip(img,1),0), kps_r

    def flip_h(self, img, kps):
        refl_matrix_h = np.array([[-1, 0], [0, 1]])
        kps_h = kps.dot(refl_matrix_h)
        kps_h[:,0] = kps_h[:,0] + self.img_size
        return np.flip(img, 1), kps_h

    def flip_v(self, img, kps):
        refl_matrix_v = np.array([[1, 0], [0, -1]])
        kps_h = kps.dot(refl_matrix_v)
        kps_h[:,1] = kps_h[:,1] + self.img_size
        return np.flip(img, 0), kps_h

    def select_transform(self, image, landmarks):
        selected = self.prob.rvs()
        print(selected)
        if selected == 1:
            return self.flip(image, landmarks)
        
        if selected == 2:
            return self.flip_h(image, landmarks)

        if selected == 3:
            return self.flip_v(image, landmarks)

        return image, landmarks


    def __getitem__(self, idx):
        row = self.landmarks_frame.loc[idx]
        img_name = row[1]
        image = cv2.imread(img_name, 1)
        landmarks = row[2:].as_matrix() 
        #/ self.img_size
        landmarks = np.array(landmarks, dtype=np.float64)
        image = cv2.resize(image, (self.img_size, self.img_size))
        #transformacion a fin al azar
        image, landmarks = self.select_transform(image, landmarks.reshape((-1, 2)))
        landmarks = self.scaler.transform(landmarks.reshape((1, -1)))
        #print(landmarks.min())
        #print(landmarks.max())
        landmarks = landmarks.reshape(landmarks.shape[1]//2, 2)
        
        result = np.zeros((self.img_size, self.img_size))
        result = cv2.normalize(image, dst=result, alpha=0, beta=1,
                               norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)

       
        sample = {'image': result, 'landmarks': landmarks}
        if self.transform:
            sample = self.transform(sample)

        return sample
