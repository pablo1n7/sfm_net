
from visdom import Visdom
import matplotlib.pyplot as plt
import numpy as np

def show_landmarks(image, landmarks, size=1):
    """Show image with landmarks"""
    landmarks = landmarks.cpu().numpy()
    plt.imshow(image.cpu().numpy(), cmap=plt.cm.gray)
    plt.scatter(landmarks[:, 0] * size, landmarks[:, 1]
                * size, s=0.7, marker='.', c='r')
    plt.pause(0.001)

class VisdomLinePlotter(object):
    """Plots to Visdom"""
    def __init__(self, env_name='main'):
        self.viz = Visdom()
        self.env = env_name
        self.plots = {}
        self.scores_window = None
        self.surf_point_windows = None

    def plot(self, var_name, split_name, x, y, x_label='Epochs'):

        if var_name not in self.plots:
            self.plots[var_name] = self.viz.line(X=np.array([x, x]), Y=np.array([y, y]),
                                                 env=self.env,
                                                 opts=dict(legend=[split_name],
                                                           title=var_name,
                                                           xlabel=x_label,
                                                           ylabel=var_name))
        else:
            self.viz.updateTrace(X=np.array([x]), Y=np.array([y]), env=self.env,
                                 win=self.plots[var_name], name=split_name)

    def display_images(self, image, epoch):
        if self.scores_window is None:
            self.scores_window = self.viz.image(image,
                                                env=self.env,
                                                opts=dict(title="test",
                                                          caption='Epoch={}'.format(str(epoch))))
        else:
            self.viz.image(image,
                           win=self.scores_window,
                           env=self.env,
                           opts=dict(title="test",
                                     caption='Epoch={}'.format(str(epoch)))
                          )
        
    def display_surf_point(self, point_surf, point_surf_pred, epoch, r2):
        #r2 = r2_score(point_surf, point_surf_pred)
        X = np.concatenate((point_surf, point_surf_pred), axis=0) 
        X_color = np.concatenate((np.full((50, 3), (0, 0, 255)),
                                  np.full((50, 3), (255, 0, 0))),
                                  axis=0)

        if self.surf_point_windows is None:
            self.surf_point_windows = self.viz.scatter(X=X,
                                                       env=self.env,
                                                       opts=dict(markersize=10, 
                                                                 markercolor=X_color,
                                                                 title="R2 "+str(r2))
                                                      )
        else:
            self.viz.scatter(X=X,
                             win=self.surf_point_windows,
                             env=self.env,
                             opts=dict(markersize=10, 
                                       markercolor=X_color,
                                       title="R2 "+ str(r2))
                            )
